var express = require('express');
var app = express();


app.set('PORT', process.env.PORT || 3000);


app.get('/', function (req, res) {
  res.status(200).send('Hello World!');
});

app.listen(app.get('PORT'), function () {
  console.log(`Example app listening on port ${app.get('PORT')}`);
  
});